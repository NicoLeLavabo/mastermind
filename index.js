

function genereCombiOrdi(tab) {
    for (let i = 0; i <= 3; i++) {
        let result = Math.round(Math.random() * (8 - 1) + 1);
        tab.push(result);
        console.log('ordi ' + tab);
    }
    return tab;

}

function saisieChiffresJoueur(userNumbers) {

    userNumbers = prompt('Veuillez entre une suite de 4 chiffres entre 1 et 8');
    return userNumbers;
}

function partieTerminee(tab, tabOrdi) {
    let bienPlace = 0;
    let malPlace = 0;
    let count = 0;
    for (let i = 0; i < tab[0].length; i++) {
        console.log(tab[0][i]);
        console.log(tabOrdi[i]);
        if (tab[0][i] == tabOrdi[i]) {
            console.log('count' + count)
            bienPlace++;
            if (bienPlace === 4) {
                console.log('Bien joué tu as gagné');
                return true;
            }
        }
        if (tabOrdi.toString().includes(tab[0][i])) {
            if (tab[0][i] != tabOrdi[i].toString()) {
                malPlace++;
            }
        }
        console.log('includes', tabOrdi.toString().includes(tab[0][i]));
        console.log('tab i ', tab[0][i].toString());
        console.log('tab ordi i ', tabOrdi[i].toString());
        console.log(tabOrdi);

    }
    console.log('Bien placé : ', bienPlace, ' et Mal placé : ', malPlace)
    return false;
}
function mastermind() {
    let tableauOrdi = [];
    let tableauJoueur = [[], [], [], [], [], [], [], [], [], [], [], []];
    let jeuGagné;
    let nbChances = 0;

    // combinaison aleatoire
    genereCombiOrdi(tableauOrdi);

    while (!partieTerminee(tableauJoueur, tableauOrdi, nbChances) || nbChances <= 2) {

        //recup entré user
        let tabSaisi = saisieChiffresJoueur(tableauJoueur[nbChances]);
        //ajouter premier element tableau entré user
        tableauJoueur.unshift(tabSaisi);
        // supprimer dernier élément tableau
        tableauJoueur.pop();

        partieTerminee(tableauJoueur, tableauOrdi);
        nbChances++;
        console.log('nb chances', nbChances);
        console.log(tableauJoueur);
    }
}

